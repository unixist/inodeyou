#!/usr/bin/env python

import os
import sys
import time
import pytsk3

def tsk_get_inode(dirent):
    inode = None
    is_dir = False
    if dirent.info.meta:
      if dirent.info.name.name in ['.', '..']:
        pass
      elif dirent.info.name.type == pytsk3.TSK_FS_NAME_TYPE_REG and \
        dirent.info.meta.type == pytsk3.TSK_FS_META_TYPE_REG:
          inode = int(dirent.info.meta.addr)
      elif dirent.info.name.type == pytsk3.TSK_FS_NAME_TYPE_DIR and \
        dirent.info.meta.type == pytsk3.TSK_FS_META_TYPE_DIR and \
        not dirent.info.name.name == '$OrphanFiles':
          #Uncomment the following code line to detect hidden directories.
          #I haven't yet reliably implemented this so you will get some false
          #positives if you use it.
          #inode = int(dirent.info.meta.addr)
          is_dir = True
    return (inode, is_dir)

def tsk_walk_path(fs, inode, inodes=set()):
  if isinstance(inode, str):
    cur = fs.open_dir(inode)
  else:
    cur = fs.open_dir(inode=inode)
  while True:
    try:
      dirent = cur.next()
      inode, is_dir = tsk_get_inode(dirent)
      if isinstance(inode, int):
        inodes.add(inode)
        if is_dir:
          tsk_walk_path(fs, inode, inodes=inodes)
    except StopIteration:
      break
  return inodes

def get_tsk_inodes(volume, root):
  img = pytsk3.Img_Info(volume)
  fs = pytsk3.FS_Info(img)
  return tsk_walk_path(fs, root)

def get_fs_inodes(path):
  inodes = set()
  path_dev = os.stat(path).st_dev
  major, minor = os.major(path_dev), os.minor(path_dev)
  for d in os.walk(path):
    st = os.stat(d[0]).st_dev
    # Only search the same device as that of 'path'
    if (major, minor) != (os.major(st), os.minor(st)):
      continue
    for f in d[2]:
      abs_f = '%s/%s' % (d[0], f)
      try:
        inodes.add(os.stat(abs_f).st_ino)
      except OSError, e:
        #Ignore dangling symlinks
        if not os.path.islink(abs_f):
          raise
    if path != d[0]:
      inodes.add(os.stat(d[0]).st_ino)
  return inodes

l = len(sys.argv)
if l > 1:
  volume = sys.argv[1]
if l > 2:
  mount_path = sys.argv[2]
if l > 3:
  root = sys.argv[3]
else:
  root = '/'

os.system('/bin/sync')
os.system('/bin/echo 3 > /proc/sys/vm/drop_caches')
tsk_inodes = get_tsk_inodes(volume, root)
fs_inodes = get_fs_inodes(mount_path)

for i in tsk_inodes - fs_inodes:
  print i
